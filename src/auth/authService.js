
import EventEmitter from 'events'
const localStorageKey = 'loggedIn';

const tokenExpiryKey = 'tokenExpiry';

class AuthService extends EventEmitter {

    logOut() {
        localStorage.removeItem('userInfo');
        $cookies.delete("accessToken");
        
    }

    isAuthenticated() {
        return (
            new Date(Date.now()) < new Date(localStorage.getItem(tokenExpiryKey)) &&
            localStorage.getItem(localStorageKey) === 'true'
        );
    }
}

export default new AuthService();
