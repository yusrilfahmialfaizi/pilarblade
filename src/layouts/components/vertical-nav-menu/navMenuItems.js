/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [

  {
    url: '/dashboard',
    name: 'Home',
    icon: 'HomeIcon',
    slug: 'home',
    i18n: 'Home',
  },
  {
    header: 'APPS',
    icon: 'PacakageIcon',
    i18n: 'Apps',
    items: [
      {
        url: '/data/article',
        name: 'Article',
        slug: 'data-article',
        icon: 'BookOpenIcon',
        i18n: 'Article',
      },
      {
        url: '/maps/here-map',
        name: 'Here Map',
        slug: 'here-map',
        icon: 'MapIcon',
        i18n: 'Here Map',
      },
      {
        url: '/notification',
        name: 'Notification',
        slug: 'notification',
        icon: 'BellIcon',
        i18n: 'Notification',
      },
      {
        url: '/kirim-whatsapp',
        name: 'Message',
        slug: 'Message',
        icon: 'MailIcon',
        i18n: 'Message',
      }
    ]
  },
  {
    name: 'Setting',
    icon: 'SettingsIcon',
    submenu: [
      {
        url: null,
        name: 'Application',
        slug: 'application',
      },
      {
        url: null,
        name: 'Areas',
        slug: 'areas',
      },
      {
        url: null,
        name: 'Companies',
        slug: 'companies',
      },
      {
        url: '/data/user',
        name: 'User Management',
        slug: 'users',
      },
      // {
      //   url: null,
      //   name: 'Role',
      //   slug: 'roles',
      // },
      {
        url: null,
        name: 'Delivery Status',
        slug: 'delivery_statuses',
      },
      {
        url: null,
        name: 'General Setting',
        slug: 'general-setting',
        submenu: [
          {
            url: null,
            name: 'Commodities',
            slug: 'commodities',
          },
          {
            url: null,
            name: 'Units',
            slug: 'units',
          },
          {
            url: null,
            name: 'Container Type',
            slug: 'container_types',
          },
          {
            url: null,
            name: 'Vehicle Type',
            slug: 'vehicle_types',
          },
          {
            url: null,
            name: 'Taxes and VAT',
            slug: 'taxes',
          },
        ]
      },
      {
        url: null,
        name: 'Operational',
        slug: 'operational',
        submenu: [
          {
            url: null,
            name: 'Costs',
            slug: 'costs',
          },
          {
            url: null,
            name: 'Routes',
            slug: 'routes',
          }
        ]
      }
    ]
  },
  {
    url: null,
    name: 'Contact',
    icon: 'UsersIcon',
    submenu: [
      {
        url: null,
        name: 'All Contacts',
        slug: 'contacts',
      },
      {
        url: null,
        name: 'Employees',
        slug: 'employee_contacts',
      },
    ]
  },
  {
    url: null,
    name: 'Vehicles',
    icon: 'TruckIcon',
    submenu: [
      {
        url: null,
        name: 'Vehicles List',
        slug: 'vehicles',
      },
    ]
  },
  {
    url: null,
    name: 'Marketing',
    icon: 'PhoneCallIcon',
    submenu: [
      {
        url: null,
        name: 'Price Lists',
        slug: 'price_lists',
      },
      {
        url: null,
        name: 'Contracts',
        slug: 'contracts',
      }
    ]
  },
  {
    url: null,
    name: 'Driver',
    icon: 'UsersIcon',
    submenu: [
      {
        url: null,
        name: 'Drivers',
        slug: 'driver_contacts',
      },
      {
        url: null,
        name: 'Driver Absences',
        slug: 'driver_absences',
      },
    ]
  },
  {
    url: null,
    name: 'Operational',
    icon: 'CpuIcon',
    submenu: [
      {
        url: null,
        name: 'Job Orders',
        slug: 'job_orders',
      },
      {
        url: null,
        name: 'Packing Lists',
        slug: 'manifests',
      },
      {
        url: null,
        name: 'Driver Shipments',
        slug: 'shipments',
      },
      {
        url: null,
        name: 'Invoice',
        slug: 'invoice',
        submenu: [
          {
            url: null,
            name: 'Invoice List',
            slug: 'invoices',
          },
          {
            url: null,
            name: 'Invoice Approval',
            slug: 'invoice-approve',
          },
        ]
      },
      {
        url: null,
        name: 'Map Shipment',
        slug: 'map_shipments',
      },
      {
        url: null,
        name: 'Customer Payment',
        slug: 'customer-payments',
        submenu: [
          {
            url: null,
            name: 'Receivables List',
            slug: 'receivables'
          },
          {
            url: null,
            name: 'Payment',
            slug: 'bills'
          }
        ]
      },
      {
        url: null,
        name: 'Vendor Payment',
        slug: 'vendor-payments',
        submenu: [
          {
            url: null,
            name: 'Payables List',
            slug: 'payables'
          },
          {
            url: null,
            name: 'Payment',
            slug: 'debts'
          }
        ]
      },
      {
        url: null,
        name: 'Reports',
        slug: 'reports',
        submenu: [
          {
            url: null,
            name: 'Car Shipment',
            slug: 'car-shipment'
          },
          {
            url: null,
            name: 'Sparepart Shipment',
            slug: 'sparepart_shipments'
          },
          {
            url: null,
            name: 'Drivers',
            slug: 'drivers'
          },
          {
            url: null,
            name: 'Driver Performance',
            slug: 'driver_performances'
          },
          {
            url: null,
            name: 'Transaction',
            slug: 'transaction'
          }
        ]
      }
    ]
  }
]


