/*=========================================================================================
  File Name: moduleAuthActions.js
  Description: Auth Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import jwt from "../../http/requests/auth/jwt/index.js"
import router from '@/router'
import { reject, resolve } from "core-js/fn/promise"

export default {
    
    loginUser({ commit }, user) {
        return new Promise((resolve,reject) => {
        jwt.login(user.email, user.password)
          .then(response => {
            // If there's user data in response
            if(response.data.user.data.userData) {
                // Navigate User to homepage
                router.push(router.currentRoute.query.to || '/dashboard')

                // Set accessToken
                // localStorage.setItem("accessToken", response.data.user.data.accessToken)
                $cookies.set("accessToken", `Bearer ${response.data.user.data.accessToken}`, 60 * 60 * 8)
                // console.log(localStorage.getItem("accessToken"));
                // Update user details
                commit('UPDATE_USER_INFO', response.data.user.data.userData, {root: true})

                // Set bearer token in axios
                commit("SET_BEARER", response.data.user.data.accessToken)

                resolve(response.data.user)
            }else {
                reject({message: response.data.user.data.message})
            }

          })
          .catch(error => { reject({message: "Wrong Email or Password", error: error}) })
        
        })
     },
  registerUserJWT({ commit }, payload) {

      console.log(payload);
      const { name, email, password, confirmPassword } = payload.userDetails

      return new Promise((resolve,reject) => {

        // Check confirm password
        if(password !== confirmPassword) {
          reject({message: "Password doesn't match. Please try again."})
        }
        jwt.registerUser(name, email, password)
          .then(response => {
            router.push({ name: 'email-verification', params: { email: email } })

            resolve(response)
          })
          .catch(error => { reject(error) })
      })
    },
    fetchAccessToken() {
      return new Promise((resolve) => {
        jwt.refreshToken().then(response => { resolve(response) })
      })
    },    
}
