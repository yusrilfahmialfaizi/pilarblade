import axios from 'axios'

const state = {};
const getters = {};
const actions = {
     loginUser({ }, user) {
          axios
               .post('http://backend.local/api/login', {
                    email: user.email,
                    password: user.password
               })
               .then(res => {
                    console.log(res);
               })
     }
};
const mutations = {};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}