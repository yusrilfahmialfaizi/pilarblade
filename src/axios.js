// axios
import axios from 'axios'

const domain = "http://backend.local/"
const headers = 'Bearer' + localStorage.getItem('accessToken')

export default axios.create({
  domain,
  // You can add your headers here
  // headers
})
