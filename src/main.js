/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import App from './App.vue'

// Vuesax Component Framework
import Vuesax from 'vuesax'
import 'material-icons/iconfont/material-icons.css' //Material Icons
import 'vuesax/dist/vuesax.css'; // Vuesax
Vue.use(Vuesax)

// require
Vue.use(require('vue-cookies'))

// es2015 module
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

// set default config
Vue.$cookies.config('7d')

// set global cookie
Vue.$cookies.set('theme','default');
Vue.$cookies.set('hover-time','1s');

// axios
import axios from "./axios.js"
Vue.prototype.$http = axios
const token = Vue.$cookies.get('token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

// API Calls
import "./http/requests"

// mock
import "./fake-db/index.js"

// Theme Configurations
import '../themeConfig.js'

// ACL
import acl from './acl/acl'

// Firebase
// import '@/firebase/firebaseConfig'

// Globally Registered Components
import './globalComponents.js'


// Styles: SCSS
import './assets/scss/main.scss'


// Tailwind
import '@/assets/css/main.css'


// Vue Router
import router from './router'


// Vuex Store
import store from './store/store'


// i18n
import i18n from './i18n/i18n'


// Vuexy Admin Filters
import './filters/filters'


// Clipboard
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard);


// Tour
import VueTour from 'vue-tour'
Vue.use(VueTour)
require('vue-tour/dist/vue-tour.css')


// VeeValidate
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);


// Google Maps
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        // Add your API key here
        key: 'YOUR_API_KEY',
        libraries: 'places', // This is required if you use the Auto complete plug-in
    },
})

// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)


// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'


// Feather font icon
require('./assets/css/iconfont.css')

// Initialize Firebase
import firebase from 'firebase/compat/app'
import "firebase/compat/messaging";

firebase.initializeApp({
  apiKey: "AIzaSyDR9CAtfdzbvtI_kNQsxPw2UyfNMbZ25aE",
  authDomain: "pilar-blade.firebaseapp.com",
  projectId: "pilar-blade",
  storageBucket: "pilar-blade.appspot.com",
  messagingSenderId: "203318277750",
  appId: "1:203318277750:web:4b6082857a794241d79157",
  measurementId: "G-71RHHFNX5G",
});

const messaging = firebase.messaging();
navigator.serviceWorker.register('firebase-messaging-sw.js').then(function(registration) {
     console.log('Service Worker Registered!', registration);
     messaging.getToken().then((token) => {
       console.log(token)
       const data = JSON.parse(localStorage.getItem("userInfo"))
       axios.put('http://backend.local/api/fcm_token/' + data.uuid, {fcm_token: token})
     })
}).catch(function(err) {
    console.error('Service Worker registration failed', err);
});
// fiebase configs end


// Vue select css
// Note: In latest version you have to add it separately
// import 'vue-select/dist/vue-select.css';


Vue.config.productionTip = false

new Vue({
    router,
    store,
    i18n,
    acl,
    render: h => h(App)
}).$mount('#app')
