import firebase from 'firebase/compat/app'
import 'firebase/compat/messaging'

var firebaseConfig = {
  apiKey: "AIzaSyDR9CAtfdzbvtI_kNQsxPw2UyfNMbZ25aE",
  authDomain: "pilar-blade.firebaseapp.com",
  projectId: "pilar-blade",
  storageBucket: "pilar-blade.appspot.com",
  messagingSenderId: "203318277750",
  appId: "1:203318277750:web:4b6082857a794241d79157",
  measurementId: "G-71RHHFNX5G",
}

firebase.initializeApp(firebaseConfig)

export default firebase.messaging()